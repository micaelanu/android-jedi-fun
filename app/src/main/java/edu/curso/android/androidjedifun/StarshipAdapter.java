package edu.curso.android.androidjedifun;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import edu.curso.android.androidjedifun.rest.Starships;

public class StarshipAdapter extends RecyclerView.Adapter<StarshipAdapter.StarshipViewHolder> {

    private Context context;
    List<Starships> starshipsList;
    private OnStarshipListener clickOnStarshipListener;

    public StarshipAdapter(Context context, List<Starships> starshipsList, OnStarshipListener onStarshipListener) {
        this.context = context;
        this.starshipsList = starshipsList;
        this.clickOnStarshipListener = onStarshipListener;
    }

    public void setStarshipsList(List<Starships> starshipsList) {
        this.starshipsList = starshipsList;
        notifyDataSetChanged();
    }

    @Override
    public StarshipAdapter.StarshipViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_starship_adapter, parent, false);
        return new StarshipViewHolder(view, clickOnStarshipListener );
    }

    @Override
    public void onBindViewHolder(StarshipAdapter.StarshipViewHolder holder, int position) {
        holder.textViewName.setText(starshipsList.get(position).getName());
        holder.textViewModel.setText(starshipsList.get(position).getModel());

    }

    @Override
    public int getItemCount() {
        if (starshipsList != null) {
            System.out.println(starshipsList.size());
            return starshipsList.size();
        }
        return 0;
    }

    public class StarshipViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView textViewName;
        TextView textViewModel;
        OnStarshipListener onStarshipListener;

        public StarshipViewHolder(View itemView, OnStarshipListener onStarshipListener) {
            super(itemView);

            textViewName = itemView.findViewById(R.id.textViewNameA);
            textViewModel = itemView.findViewById(R.id.textViewModel);
            this.onStarshipListener = onStarshipListener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onStarshipListener.onStarshipClick(getAdapterPosition());

        }
    }

    public interface OnStarshipListener{
        void onStarshipClick(int position);
    }

}
