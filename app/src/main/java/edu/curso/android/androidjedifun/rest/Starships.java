package edu.curso.android.androidjedifun.rest;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Starships implements Parcelable {

    @SerializedName("name")
    private String name;
    @SerializedName("model")
    private String model;
    @SerializedName("starship_class")
    private String starship_class;
    @SerializedName("manufacturer")
    private String manufacturer;
    @SerializedName("lenght")
    private String lenght;
    @SerializedName("crew")
    private String crew;

   public Starships(){}

    public Starships(String name, String model) {
        this.name = name;
        this.model = model;
        this.starship_class = starship_class;
        this.manufacturer = manufacturer;
        this.lenght = lenght;
        this.crew = crew;
    }

    protected Starships(Parcel in) {
        name = in.readString();
        model = in.readString();
        starship_class = in.readString();
        manufacturer = in.readString();
        lenght = in.readString();
        crew = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(model);
        dest.writeString(starship_class);
        dest.writeString(manufacturer);
        dest.writeString(lenght);
        dest.writeString(crew);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Starships> CREATOR = new Creator<Starships>() {
        @Override
        public Starships createFromParcel(Parcel in) {
            return new Starships(in);
        }

        @Override
        public Starships[] newArray(int size) {
            return new Starships[size];
        }
    };

    public String getName() { return name; }
    public void setName(String name) { this.name = name;}

    public String getModel() { return model; }
    public void setModel(String model) { this.model = model;}

    public String getStarship_class() { return starship_class; }
    public void setStarship_class(String starship_class) { this.starship_class = starship_class;}

    public String getManufacturer() { return manufacturer; }
    public void setManufacturer(String manufacturer) { this.manufacturer = manufacturer;}

    public String getLenght() { return lenght; }
    public void setLenght(String lenght) { this.lenght = lenght;}

    public String getCrew() { return crew; }
    public void setCrew(String crew) { this.crew = crew;}




}
