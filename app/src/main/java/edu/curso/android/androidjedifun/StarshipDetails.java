package edu.curso.android.androidjedifun;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;

import edu.curso.android.androidjedifun.rest.PostService;
import edu.curso.android.androidjedifun.rest.RetrofitClient;
import edu.curso.android.androidjedifun.rest.Starships;
import edu.curso.android.androidjedifun.rest.StarshipsPost;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StarshipDetails extends AppCompatActivity {

   private ArrayList<Starships> starships;

    private TextView textViewNameA;
    private TextView textViewNameB;
    private TextView textViewModelA;
    private TextView textViewModelB;
    private TextView textViewClassA;
    private TextView textViewClassB;
    private TextView textViewManufacturerA;
    private TextView textViewManufacturerB;
    private TextView textViewLengthA;
    private TextView textViewLengthB;
    private TextView textViewCrewSizeA;
    private TextView textViewCrewSizeB;

    private Starships starshipsDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        starshipsDetails = getIntent().getExtras().getParcelable("starship");
        setContentView(R.layout.activity_starship_details);

        Toolbar toolbar = findViewById(R.id.toolbarStarshipDetails );
        toolbar.setTitle("Details");
        setSupportActionBar(toolbar);

        findTextViewById();
        setDatos();

    }





    public void findTextViewById(){
        textViewNameA = findViewById(R.id.textViewNameA);
        textViewNameB =  findViewById(R.id.textViewNameB);
        textViewModelA = findViewById(R.id.textViewModelA);
        textViewModelB = findViewById(R.id.textViewModelB);
        textViewClassA = findViewById(R.id.textViewClassA);
        textViewClassB = findViewById(R.id.textViewClassB);
        textViewManufacturerA= findViewById(R.id.textViewManufacturerA);
        textViewManufacturerB= findViewById(R.id.textViewManufacturerB);
        textViewLengthA = findViewById(R.id.textViewLengthA);
        textViewLengthB = findViewById(R.id.textViewLengthB);
        textViewCrewSizeA = findViewById(R.id.textViewCrewSizeA);
        textViewCrewSizeB = findViewById(R.id.textViewCrewSizeB);
    }

    public void setDatos(){
        textViewNameB.setText(starshipsDetails.getName());
        textViewModelB.setText(starshipsDetails.getModel());
        textViewClassB.setText(starshipsDetails.getStarship_class());
        textViewManufacturerB.setText(starshipsDetails.getManufacturer());
        textViewLengthB.setText(starshipsDetails.getLenght());
        textViewCrewSizeB.setText(starshipsDetails.getCrew());
    }




}
