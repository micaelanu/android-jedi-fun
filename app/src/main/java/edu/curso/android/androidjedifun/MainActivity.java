package edu.curso.android.androidjedifun;

import android.content.Intent;
import android.os.Bundle;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import android.util.Log;

import edu.curso.android.androidjedifun.rest.PostService;
import edu.curso.android.androidjedifun.rest.RetrofitClient;
import edu.curso.android.androidjedifun.rest.StarshipsPost;
import edu.curso.android.androidjedifun.rest.Starships;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity implements StarshipAdapter.OnStarshipListener {

    private ArrayList<Starships> clickStarships = new ArrayList<>();

    List<Starships> starshipsList;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbarStarshipMain);
        toolbar.setTitle("Starships");
        setSupportActionBar(toolbar);


        recyclerView = findViewById(R.id.recyclerViewStarships);

        llamarDatos();

        LinearLayoutManager layoutManager= new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
    }

    public void llamarDatos(){
        PostService postService = RetrofitClient.recuperarRetrofit().create(PostService.class);
        Call<StarshipsPost> call = postService.getStarships();

        call.enqueue(new Callback<StarshipsPost>() {

            @Override
            public void onResponse(Call<StarshipsPost> call, Response<StarshipsPost> response) {

                starshipsList = response.body().getStarshipsResults();
                Log.d("TAG", "Response = " + starshipsList);
                iniciarAdapter(starshipsList);
            }

            @Override
            public void onFailure(Call<StarshipsPost> call, Throwable t) {
                Log.v("TAG","Response = "+t.toString());
            }
        });
    }

    public void iniciarAdapter(List<Starships> starshipsList){

        StarshipAdapter starshipAdapter = new StarshipAdapter(this,starshipsList,this);
        recyclerView.setAdapter(starshipAdapter);
    }

    @Override
    public void onStarshipClick(int position) {

        Log.d("TAG","onStarshipClickListener: clicked." + position);
        Intent intent = new Intent(this,StarshipDetails.class);
        intent.putExtra("starship",starshipsList.get(position));
        startActivity(intent);


    }


}

