package edu.curso.android.androidjedifun.rest;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StarshipsPost {

    @SerializedName("results")
    private List<Starships> starships;

    public List<Starships> getStarshipsResults() { return starships;}
}
